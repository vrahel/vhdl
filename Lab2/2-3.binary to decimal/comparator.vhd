LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY comparator IS
	PORT( inputs : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		   Z		 : OUT STD_LOGIC);	
END comparator;

ARCHITECTURE comp_implementation OF comparator IS
	
	SIGNAL inputs_ptr : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL z_ptr		: STD_LOGIC;
	
	BEGIN
		inputs_ptr <= inputs;
		PROCESS(inputs_ptr, z_ptr) -- z_ptr is used to have knowledge of whether the number is strictly greater than 9, or not
			BEGIN
				IF(inputs_ptr > "1001") THEN
					z_ptr <= '1';
				ELSE
					z_ptr <= '0';
				END IF;
		END PROCESS;
		
		z <= z_ptr;
	
END comp_implementation;