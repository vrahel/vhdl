LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY binarytodecimal IS
	PORT( SW         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			HEX0, HEX1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END binarytodecimal;

ARCHITECTURE btd_implementation OF binarytodecimal IS 
	COMPONENT comparator
		PORT( inputs : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
				Z	    : OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT circuitA
		PORT( V 			: IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
				REMINDER	: OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT mux_4bit 
		PORT( INPUTS_X : IN  STD_LOGIC_VECTOR (3 DOWNTO 0);
				INPUTS_Y : IN  STD_LOGIC_VECTOR (3 DOWNTO 0);
				S	      : IN  STD_LOGIC;
				M	      : OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT char_7seg
		PORT( C	 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
				HEX : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT CIRCUITB 
		PORT( IS_OVER_TEN : IN STD_LOGIC;
				HEX         : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;
	
	
	SIGNAL z_ptr 			: std_logic;
	signal mux_in_ptr_X, mux_in_ptr_Y, mux_out_ptr : std_logic_vector(3 DOWNTO 0);
	
	BEGIN
		mux_in_ptr_X <= SW; 
		mux_in_ptr_Y(3) <= '0'; -- 
		
		COMP_MAP		: comparator PORT MAP(mux_in_ptr_X, z_ptr);
		CIRCUIT_MAP	: circuitA 	 PORT MAP(mux_in_ptr_X(2 DOWNTO 0), mux_in_ptr_Y(2 DOWNTO 0));
		MUX_4BIT_MAP: mux_4bit	 PORT MAP(mux_in_ptr_X, mux_in_ptr_Y, z_ptr, mux_out_ptr);
		
		D0: char_7seg  PORT MAP(mux_out_ptr, HEX0);
		D1: CIRCUITB   PORT MAP(z_ptr, HEX1);
	

END btd_implementation;