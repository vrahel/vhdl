LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY circuitB IS
	PORT( IS_OVER_TEN : IN STD_LOGIC;
			HEX         : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END circuitB;

ARCHITECTURE CIRCUITB_IMPL OF circuitB IS

	BEGIN
		HEX(0) <= IS_OVER_TEN; -- This is used to map decimal 0 and 1 on the HEX1 display, the only two decimal numbers that can be present
		HEX(1) <= '0';         -- on said display (4 bits, so it goes only up to 15)
		HEX(2) <= '0';
		HEX(3) <= IS_OVER_TEN;
		HEX(4) <= IS_OVER_TEN;
		HEX(5) <= IS_OVER_TEN;
		HEX(6) <= '1';

END CIRCUITB_IMPL;