LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_4bit IS
	PORT ( INPUTS_X : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
			 INPUTS_Y : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
			 S			 : IN STD_LOGIC;
			 M			 : OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
END mux_4bit;

ARCHITECTURE MUX_4BIT_2_TO_1_IMPLEMENTATION OF mux_4bit IS
	COMPONENT mux
		PORT ( A, B : IN STD_LOGIC;
				   S  : IN STD_LOGIC;
				   M  : OUT STD_LOGIC);
	END COMPONENT;
	
	SIGNAL X, Y	: STD_LOGIC_VECTOR (3 DOWNTO 0);
	SIGNAL SEL	: STD_LOGIC;
	
	BEGIN
	--modified version, check original if needed (inputs splitted in x and y array)
		X <= INPUTS_X;
		Y <= INPUTS_Y;
		
		mux_4bit_ARRAY_LOOP:
		FOR I IN 3 DOWNTO 0 GENERATE
			mux_4bit_ELEMENT: mux PORT MAP(X(I), Y(I), S, M(I));
		END GENERATE mux_4bit_ARRAY_LOOP;
END MUX_4BIT_2_TO_1_IMPLEMENTATION;