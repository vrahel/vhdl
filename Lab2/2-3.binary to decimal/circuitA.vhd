LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY circuitA IS
	PORT( V			: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
			REMINDER	: OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
END circuitA;

ARCHITECTURE CIRCUIT_A_IMPL OF circuitA IS
	BEGIN
	--adding + 110, explained in report
	REMINDER <= V + "110";

END CIRCUIT_A_IMPL;