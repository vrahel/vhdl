LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux IS
   PORT
   ( S		: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     OUTPUT : OUT STD_LOGIC_VECTOR(14 DOWNTO 0));
END mux;

ARCHITECTURE mux_implementation OF mux IS
SIGNAL h, e, l, o, c, f, p : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL sel                 : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN
	h <= "000";    -- Each letter is associated to a 3-bit signal
	e <= "001";
	l <= "010";
	o <= "011";
	c <= "100";
	f <= "101";
	p <= "110"; 
	sel <= S;
   PROCESS (h,e,l,o,c,f,p,sel) IS
   BEGIN
      CASE sel IS
         WHEN "00"  => OUTPUT <= o & l & l & e & h;
         WHEN "01"  => OUTPUT <= o & p & p & e & c;
         WHEN "10"  => OUTPUT <= o & l & l & e & c;
         WHEN "11"  => OUTPUT <= o & p & p & e & f;
      END CASE;
   END PROCESS;
END mux_implementation;