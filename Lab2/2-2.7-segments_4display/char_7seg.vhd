LIBRARY ieee;
USE ieee.STD_LOGIC_1164.all;

ENTITY char_7seg IS
	PORT
	(  -- Input ports
		C		:	IN STD_LOGIC_VECTOR(2 DOWNTO 0);

		-- Output ports
		HEX	:	OUT STD_LOGIC_VECTOR(0 TO 6));
END char_7seg;

ARCHITECTURE char_7seg_implementation OF char_7seg IS
	BEGIN		
		HEX(0) <= (NOT(C(0)) AND NOT(C(2))) OR (C(2) AND C(1) AND C(0));
		HEX(1) <= (NOT(C(2)) AND C(1) AND NOT(C(0))) OR (NOT(C(1)) AND C(0)) OR (C(2) AND NOT(C(1))) OR (C(2) AND (C(0)));
		HEX(2) <= (C(1) AND NOT(C(0))) OR (NOT(C(1)) AND C(0)) OR C(2);
		HEX(3) <= (NOT(C(2)) AND NOT(C(1)) AND NOT(C(0))) OR (C(2) AND C(1)) OR (C(2) AND C(0));
		HEX(4) <= C(2) AND C(1) AND C(0);
		HEX(5) <= C(2) AND C(1) AND C(0);
		HEX(6) <= (NOT(C(2)) AND C(1)) OR (C(2) AND NOT(C(1)) AND NOT(C(0))) OR (C(1) AND C(0));
END char_7seg_implementation;