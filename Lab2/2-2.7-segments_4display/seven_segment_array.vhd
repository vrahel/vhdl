LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY seven_segment_array IS 
	PORT 
	( SW                           : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
	  HEX0, HEX1, HEX2, HEX3, HEX4 : OUT STD_LOGIC_VECTOR(0 TO 6) -- Declaration of the five 7-segments displays
	);
END seven_segment_array;

ARCHITECTURE Behavior OF seven_segment_array IS
   COMPONENT mux IS 
		PORT ( S	     : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
				 OUTPUT : OUT STD_LOGIC_VECTOR(14 DOWNTO 0));
	END COMPONENT;
		
	COMPONENT shifter IS
		PORT ( input  : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
				 s	     : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
			    output : OUT STD_LOGIC_VECTOR(14 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT char_7seg
		PORT ( C   : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
				 HEX : OUT STD_LOGIC_VECTOR(0 TO 6));
	END COMPONENT;

	
	SIGNAL a1, a2 : STD_LOGIC_VECTOR(14 DOWNTO 0); -- a1 is the input word of the shifter, a2 is the shifted by the desired positions,
                                                  -- or not shifted at all
	BEGIN
	MUX0   : mux PORT MAP (SW(1 DOWNTO 0),a1);
	SHIFT0 : shifter PORT MAP (a1, SW(4 DOWNTO 2),a2);
	H0     : char_7seg PORT MAP (a2(2 DOWNTO 0), HEX0);
	H1     : char_7seg PORT MAP (a2(5 DOWNTO 3), HEX1);
	H2     : char_7seg PORT MAP (a2(8 DOWNTO 6), HEX2);
	H3     : char_7seg PORT MAP (a2(11 DOWNTO 9), HEX3);
	H4     : char_7seg PORT MAP (a2(14 DOWNTO 12), HEX4);
END Behavior;