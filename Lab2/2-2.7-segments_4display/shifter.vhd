LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY shifter IS
	PORT 
	( input  : IN  STD_LOGIC_VECTOR(14 DOWNTO 0);
	  s		: IN  STD_LOGIC_VECTOR(2  DOWNTO 0);
	  output : OUT STD_LOGIC_VECTOR(14 DOWNTO 0));
END shifter;

ARCHITECTURE Behavior OF shifter IS
	SIGNAL word : STD_LOGIC_VECTOR(14 DOWNTO 0);
	SIGNAL letter0, letter1, letter2, letter3, letter4 : STD_LOGIC_VECTOR(2 DOWNTO 0);
	
	BEGIN
		letter0 <= input(2 DOWNTO 0);
		letter1 <= input(5 DOWNTO 3);
		letter2 <= input(8 DOWNTO 6);
		letter3 <= input(11 DOWNTO 9);
		letter4 <= input(14 DOWNTO 12);
		
		PROCESS(word, letter0, letter1, letter2, letter3, letter4, s) IS		-- These cases are used to shift the letters
			BEGIN
				CASE s IS
					WHEN "000"  => word <= letter0 & letter1 & letter2 & letter3 & letter4;
					WHEN "001"  => word <= letter1 & letter2 & letter3 & letter4 & letter0;
					WHEN "010"  => word <= letter2 & letter3 & letter4 & letter0 & letter1;
					WHEN "011"  => word <= letter3 & letter4 & letter0 & letter1 & letter2;
					WHEN "100"  => word <= letter4 & letter0 & letter1 & letter2 & letter3;
					WHEN OTHERS => word <= letter0 & letter1 & letter2 & letter3 & letter4;
				END CASE;
		END PROCESS;
	
	output <= word;	
	
END Behavior;