LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( -- Input ports
		   C		:	IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		   -- Output ports
		   HEX	:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0));	
END char_7seg;

ARCHITECTURE char_7seg_implementation OF char_7seg IS
	BEGIN
		HEX(0) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(2) AND NOT(C(1)) AND NOT(C(0))) OR (NOT(C(3)) AND NOT(C(2)) AND NOT(C(1)) AND C(0));
		HEX(1) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(2) AND NOT(C(1)) AND C(0)) OR (C(2) AND C(1) AND NOT(C(0)));
		HEX(2) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (NOT(C(2)) AND C(1) AND NOT(C(0)));
		HEX(3) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(2) AND NOT(C(1)) AND NOT(C(0))) OR (C(2) AND C(1) AND C(0)) OR (NOT(C(3)) AND NOT(C(2)) AND NOT(C(1)) AND C(0));
		HEX(4) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(3) AND C(0)) OR (NOT(C(3)) AND C(0)) OR (NOT(C(3)) AND C(2) AND NOT(C(1)));
		HEX(5) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(1) AND (C(0))) OR (NOT(C(3)) AND NOT(C(2)) AND NOT(C(1)) AND C(0)) OR (NOT(C(2)) AND C(1) AND NOT(C(0)));
		HEX(6) <= (C(3) AND C(1)) OR (C(3) AND C(2)) OR (C(2) AND C(1) AND C(0)) OR (NOT(C(3)) AND NOT(C(2)) AND NOT(C(1)));
		
END char_7seg_implementation;