LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY binary_bcd IS
	PORT( SW         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
			HEX0, HEX1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));	
END binary_bcd;

ARCHITECTURE binary_bcd_implementation OF binary_bcd IS
	COMPONENT char_7seg IS
		PORT( -- Input ports
			   C 	 :	IN STD_LOGIC_VECTOR(3 DOWNTO 0);
				-- Output ports
			   HEX : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;

	SIGNAL TENS   : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL DIGITS : STD_LOGIC_VECTOR(3 DOWNTO 0);
	
	BEGIN		
		CHECK_IF_OVER_FOUR:PROCESS(SW)		
			VARIABLE Z: STD_LOGIC_VECTOR(13 DOWNTO 0); -- This process is used to implement the Double-Dabble algorithm,
			BEGIN													 -- which is explained in the report
				Z := "00000000000000";
				Z(8 DOWNTO 3) := SW;
				FOR I IN 0 TO 2 LOOP
					IF Z(9 DOWNTO 6) > "0100" THEN
						Z(9 DOWNTO 6) := Z(9 DOWNTO 6) + "0011";
					END IF;
					Z := Z(12 DOWNTO 0) & '0';
				END LOOP;
				DIGITS <= Z(9 DOWNTO 6);			
				TENS   <= Z(13 DOWNTO 10);
		END PROCESS;
		
		DISPLAY_DIGITS: char_7seg PORT MAP( DIGITS, HEX0);
		DISPLAY_TENS  : char_7seg PORT MAP( TENS, HEX1);

END binary_bcd_implementation;