LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY four_bit_skip_adder IS
	GENERIC( N : INTEGER := 4);
	PORT( X, Y         : IN SIGNED(3 DOWNTO 0);
			CLOCK, RESET : STD_LOGIC;
		   C_in         : IN STD_LOGIC;
		   C_out        : OUT STD_LOGIC;
		   S            : OUT SIGNED(3 DOWNTO 0));
END four_bit_skip_adder;

ARCHITECTURE four_bit_skip_adder_implementation OF four_bit_skip_adder IS

	-- Components declarations
	COMPONENT regn IS
		PORT(	R             :  IN SIGNED(N-1 DOWNTO 0);
				Clock, Resetn : IN STD_LOGIC;
				Q             :  OUT SIGNED(N-1 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT FULL_ADDER IS
		PORT( X, Y, C_in : IN STD_LOGIC;
			    C_out, S  : OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT mux IS
		PORT( A, B, S : IN STD_LOGIC;
			   M       : OUT STD_LOGIC);
	END COMPONENT;

	SIGNAL CARRY           : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL PROPAGATE       : STD_LOGIC;
	SIGNAL X_in, Y_in      : SIGNED(3 DOWNTO 0);
	SIGNAL RESULT          : SIGNED(3 DOWNTO 0) := (others => '0');
	SIGNAL PROPAGATE_CARRY : STD_LOGIC_VECTOR(3 DOWNTO 0);

	BEGIN
	
		-- Calculate eventual carry with pure combinational circuits
		PROPAGATE_CARRY(0) <= (X_in(0) AND Y_in(0)) OR (C_in AND X_in(0)) OR (C_in AND Y_in(0));
		PROPAGATE_CARRY(1) <= (X_in(1) AND Y_in(1)) OR (PROPAGATE_CARRY(0) AND X_in(1)) OR (PROPAGATE_CARRY(0) AND Y_in(1));
		PROPAGATE_CARRY(2) <= (X_in(2) AND Y_in(2)) OR (PROPAGATE_CARRY(1) AND X_in(2)) OR (PROPAGATE_CARRY(1) AND Y_in(2));
		PROPAGATE_CARRY(3) <= (X_in(3) AND Y_in(3)) OR (PROPAGATE_CARRY(2) AND X_in(3)) OR (PROPAGATE_CARRY(2) AND Y_in(3));
	
		-- Eventually propagate carry
		PROPAGATE <= (X_in(0) XOR Y_in(0)) AND (X_in(1) XOR Y_in(1)) AND (X_in(2) XOR Y_in(2)) AND (X_in(3) XOR Y_in(3));
		MUX_FFA : mux PORT MAP(PROPAGATE_CARRY(3), C_in, PROPAGATE, C_out);
				
	
		-- Mapping ports' components
		REGX : regn PORT MAP(X, CLOCK, RESET, X_in);
		REGY : regn PORT MAP(Y, CLOCK, RESET, Y_in);
		REGS : regn PORT MAP(RESULT, CLOCK, RESET, S);

		-- Sequential adders, driven by clocked inputs
		FA0_SEQ : full_adder PORT MAP(X_in(0), Y_in(0), C_in, CARRY(0), RESULT(0));
		FA1_SEQ : full_adder PORT MAP(X_in(1), Y_in(1), CARRY(0), CARRY(1), RESULT(1));
		FA2_SEQ : full_adder PORT MAP(X_in(2), Y_in(2), CARRY(1), CARRY(2), RESULT(2));
		FA3_SEQ : full_adder PORT MAP(X_in(3), Y_in(3), CARRY(2), CARRY(3), RESULT(3));
		
END four_bit_skip_adder_implementation;