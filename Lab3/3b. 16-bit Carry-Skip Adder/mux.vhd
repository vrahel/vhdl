LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux IS
	PORT( A, B, S : IN STD_LOGIC;
		   M       : OUT STD_LOGIC);
END mux;

ARCHITECTURE mux_implementation OF mux IS
	BEGIN
		PROCESS(A,B,S)
			BEGIN
				IF(S = '0') THEN
					M <= A;
				ELSE M <= B;
				END IF;
		END PROCESS;
END mux_implementation;