LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY skip_adder IS
	PORT( X, Y          : IN SIGNED(15 DOWNTO 0);
			 CLOCK, RESET : IN STD_LOGIC;
		    S            : OUT SIGNED(15 DOWNTO 0));
END skip_adder;

ARCHITECTURE skip_adder_implementation OF skip_adder IS

	COMPONENT four_bit_skip_adder is
		PORT( X, Y         : IN SIGNED(3 DOWNTO 0);
				CLOCK, RESET : STD_LOGIC;
				C_in         : IN STD_LOGIC;
				C_out        : OUT STD_LOGIC;
				S            : OUT SIGNED(3 DOWNTO 0));
	END COMPONENT;

	SIGNAL CARRY : STD_LOGIC_VECTOR(3 DOWNTO 0);

	BEGIN
	
		FOUR_BIT_SA0 : four_bit_skip_adder PORT MAP(X(3 DOWNTO 0), Y(3 DOWNTO 0), CLOCK, RESET, '0', CARRY(0), S(3 DOWNTO 0));
		FOUR_BIT_SA1 : four_bit_skip_adder PORT MAP(X(7 DOWNTO 4), Y(7 DOWNTO 4), CLOCK, RESET, CARRY(0), CARRY(1), S(7 DOWNTO 4));
		FOUR_BIT_SA2 : four_bit_skip_adder PORT MAP(X(11 DOWNTO 8), Y(11 DOWNTO 8), CLOCK, RESET, CARRY(1), CARRY(2), S(11 DOWNTO 8));
		FOUR_BIT_SA3 : four_bit_skip_adder PORT MAP(X(15 DOWNTO 12), Y(15 DOWNTO 12), CLOCK, RESET, CARRY(2), CARRY(3), S(15 DOWNTO 12));
	
END skip_adder_implementation;