LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY select_adder IS
	PORT( X, Y         : IN SIGNED(15 DOWNTO 0);
			CLOCK, RESET : IN STD_LOGIC;
		   S            : OUT SIGNED(15 DOWNTO 0));
END select_adder;

ARCHITECTURE select_adder_implementation OF select_adder IS

	COMPONENT four_bit_fa IS
		PORT( X, Y         : IN SIGNED(3 DOWNTO 0);				
				C_in         : IN STD_LOGIC;
				C_out        : OUT STD_LOGIC;
				CLOCK, RESET : IN STD_LOGIC;
			   S            : OUT SIGNED(3 DOWNTO 0));
	END COMPONENT;
	
	SIGNAL CARRY : STD_LOGIC_VECTOR(3 DOWNTO 0);
	
	BEGIN
		
		FOUR_BIT_SA0 : four_bit_fa PORT MAP(X(3 DOWNTO 0), Y(3 DOWNTO 0), '0', CARRY(0), CLOCK, RESET, S(3 DOWNTO 0));
		FOUR_BIT_SA1 : four_bit_fa PORT MAP(X(7 DOWNTO 4), Y(7 DOWNTO 4), CARRY(0), CARRY(1), CLOCK, RESET, S(7 DOWNTO 4));
		FOUR_BIT_SA2 : four_bit_fa PORT MAP(X(11 DOWNTO 8), Y(11 DOWNTO 8), CARRY(1), CARRY(2), CLOCK, RESET, S(11 DOWNTO 8));
		FOUR_BIT_SA3 : four_bit_fa PORT MAP(X(15 DOWNTO 12), Y(15 DOWNTO 12), CARRY(2), CARRY(3), CLOCK, RESET, S(15 DOWNTO 12));
	
END select_adder_implementation;