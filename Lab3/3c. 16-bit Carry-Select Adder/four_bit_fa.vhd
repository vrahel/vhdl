LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY four_bit_fa IS
	PORT( X, Y         : IN SIGNED(3 DOWNTO 0);				
		   C_in         : IN STD_LOGIC;
		   C_out        : OUT STD_LOGIC;
			CLOCK, RESET : IN STD_LOGIC;
		   S            : OUT SIGNED(3 DOWNTO 0));
END four_bit_fa;

ARCHITECTURE four_bit_fa_implementation OF four_bit_fa IS

	-- Component declarations
	COMPONENT FULL_ADDER IS
		PORT(	X, Y  : IN STD_LOGIC;
			   C_in	: IN STD_LOGIC;
			   C_out	: OUT STD_LOGIC;
			   S		: OUT STD_LOGIC);
	END COMPONENT;
		
	COMPONENT mux IS
		PORT( A, B, S : IN STD_LOGIC;
			   M       : OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT regn IS
	GENERIC ( N : INTEGER := 4);
		PORT( R             : IN SIGNED(N-1 DOWNTO 0);
				Clock, Resetn : IN STD_LOGIC;
				Q             : OUT SIGNED(N-1 DOWNTO 0));
	END COMPONENT;

	-- Signal declarations
	SIGNAL C_0, C_1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL S_0, S_1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL X_in, Y_in, RESULT : SIGNED(3 DOWNTO 0);
	
	BEGIN	
	
	-- Mapping ports' components
	REGX : regn PORT MAP(X, CLOCK, RESET, X_in);
	REGY : regn PORT MAP(Y, CLOCK, RESET, Y_in);
	REGS : regn PORT MAP(RESULT, CLOCK, RESET, S);
	
	-- Instantiating full adders for carry 0
	FA0_FOR_0: FULL_ADDER PORT MAP(X_in(0), Y_in(0), '0', C_0(0), S_0(0));
	FA1_FOR_0: FULL_ADDER PORT MAP(X_in(1), Y_in(1), C_0(0), C_0(1), S_0(1));
	FA2_FOR_0: FULL_ADDER PORT MAP(X_in(2), Y_in(2), C_0(1), C_0(2), S_0(2));
	FA3_FOR_0: FULL_ADDER PORT MAP(X_in(3), Y_in(3), C_0(2), C_0(3), S_0(3));
	
	-- Instantiating full adders for carry 1
	FA0_FOR_1: FULL_ADDER PORT MAP(X_in(0), Y_in(0), '1', C_1(0), S_1(0));
	FA1_FOR_1: FULL_ADDER PORT MAP(X_in(1), Y_in(1), C_1(0), C_1(1), S_1(1));
	FA2_FOR_1: FULL_ADDER PORT MAP(X_in(2), Y_in(2), C_1(1), C_1(2), S_1(2));
	FA3_FOR_1: FULL_ADDER PORT MAP(X_in(3), Y_in(3), C_1(2), C_1(3), S_1(3));
	
	
	-- Multiplexing to choose the correct group of full adders
	MUX_S0: mux PORT MAP(S_0(0), S_1(0), C_in, RESULT(0));
	MUX_S1: mux PORT MAP(S_0(1), S_1(1), C_in, RESULT(1));
	MUX_S2: mux PORT MAP(S_0(2), S_1(2), C_in, RESULT(2));
	MUX_S3: mux PORT MAP(S_0(3), S_1(3), C_in, RESULT(3));
	
	-- Choosing which carry to propagate
	MUX_CARRY_OUT: mux PORT MAP(C_0(3), C_1(3), C_in, C_out);
	
END four_bit_fa_implementation;