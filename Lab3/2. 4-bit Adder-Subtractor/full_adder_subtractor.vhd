LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY full_adder_subtractor IS
	PORT(	X, Y, C_in : IN STD_LOGIC;
	      C_out      : OUT STD_LOGIC;
			SWITCH     : IN STD_LOGIC;
			S	        : OUT STD_LOGIC);
END full_adder_subtractor;

ARCHITECTURE implementation OF full_adder_subtractor IS 
	BEGIN
		ADD_SUB : PROCESS(SWITCH, X, Y, C_in) IS
			BEGIN
				IF(SWITCH <= '0') THEN -- SUM
					S <= (X XOR Y) XOR C_in;
					C_out <= (X AND Y) OR (C_in AND X) OR (C_in AND Y);
				ELSE -- SUBTRACTION
					S <= (X XOR Y) XOR C_in;
					C_out <= ((NOT X) AND Y) OR ((NOT X) AND C_in) OR (Y AND C_in);
				END IF;
			END PROCESS ADD_SUB;
END implementation;