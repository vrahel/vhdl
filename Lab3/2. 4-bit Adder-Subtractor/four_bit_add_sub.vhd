LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

-- Include package overflow
LIBRARY work;  
USE work.overflow_package.all; 

ENTITY four_bit_add_sub IS
	GENERIC ( N : INTEGER := 4);
	PORT(	
			SW		           : IN SIGNED(8 DOWNTO 0); -- SW(3) AND SW(7) are the operands' signs
			KEY	           : IN STD_LOGIC_VECTOR(1 DOWNTO 0); -- RESET KEY(0); CLOCK KEY(1)
			LEDR             : OUT STD_LOGIC_VECTOR(9 DOWNTO 9); -- LEDR(9) is used to display an eventual overflow
			RES 			     : OUT SIGNED(N-1 DOWNTO 0);
			HEX0, HEX1, HEX2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0) -- These show respectively the two operands and the sum
		); 
END four_bit_add_sub;


ARCHITECTURE implementation OF four_bit_add_sub IS 
	COMPONENT regn IS
		PORT(	R             : IN SIGNED(N-1 DOWNTO 0);
				Clock, Resetn : IN STD_LOGIC;
				Q             : OUT SIGNED(N-1 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT flipflop IS
		PORT ( D, Clock, Resetn : IN STD_LOGIC;
				 Q						: OUT STD_LOGIC);
	END COMPONENT;

	COMPONENT full_adder_subtractor IS
	PORT(	X, Y, C_in : IN STD_LOGIC;
	      C_out      : OUT STD_LOGIC;
			SWITCH     : IN STD_LOGIC;
			S	        : OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT char_7seg IS
		PORT ( C	  : IN SIGNED(3 DOWNTO 0);
			    HEX : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;
	
	-- Signals declarations
	SIGNAL RESET      : STD_LOGIC;
	SIGNAL CLOCK      : STD_LOGIC;
	SIGNAL X_in, Y_in : SIGNED(N-1 DOWNTO 0); -- Signals used as the full adder inputs
	SIGNAL C1, C2, C3 : STD_LOGIC; -- Carry bits for the full adders
	SIGNAL C_out      : STD_LOGIC;
	
	SIGNAL X    : SIGNED(N-1 DOWNTO 0); -- X and Y are the input operands, passed to their registers
	SIGNAL Y    : SIGNED(N-1 DOWNTO 0); -- for synchronization purposes
	SIGNAL S	   : SIGNED(N-1 DOWNTO 0) := (OTHERS => '0'); -- The sum vector is initialized as all zeroes
	SIGNAL C_in	: STD_LOGIC := '0'; -- The input carry is a fixed value = 0


	SIGNAL RESULT  : SIGNED(N-1 DOWNTO 0) := (OTHERS => '0'); -- This is the final sum, output of the register
	SIGNAL OFW     : STD_LOGIC;
	SIGNAL ADD_SUB : STD_LOGIC; -- Signal used to decide whether to sum or subtract the operands (0 for sum, 1 for subtraction)
	
	-- Main process
	BEGIN
		
		RESET <= KEY(0);
		CLOCK <= KEY(1);
		X <= SW(3 DOWNTO 0);
		Y <= SW(7 DOWNTO 4);
		ADD_SUB <= SW(8);		
		
		OFW <= is_overflow(C_out, C3); -- Checking the last two carries with a XOR, to verify if an overflow is present
		
		-- Mapping ports' components		
		REGX : regn PORT MAP(X, CLOCK, RESET, X_in);
		REGY : regn PORT MAP(Y, CLOCK, RESET, Y_in);
		REGS : regn PORT MAP(S, CLOCK, RESET, RESULT);

		
		FAS0 : full_adder_subtractor PORT MAP(X_in(0), Y_in(0), C_in , C1, ADD_SUB, S(0));
		FAS1 : full_adder_subtractor PORT MAP(X_in(1), Y_in(1), C1, C2, ADD_SUB, S(1));
		FAS2 : full_adder_subtractor PORT MAP(X_in(2), Y_in(2), C2, C3, ADD_SUB, S(2));
		FAS3 : full_adder_subtractor PORT MAP(X_in(3), Y_in(3), C3, C_out, ADD_SUB, S(3));
		DFF : flipflop PORT MAP(OFW, CLOCK, RESET, LEDR(9));
		
		
		FIRST_OPERAND_DISPLAY  : char_7seg  PORT MAP(X, HEX0);
		SECOND_OPERAND_DISPLAY : char_7seg  PORT MAP(Y, HEX1);
		RESULT_DISPLAY			  : char_7seg  PORT MAP(RESULT, HEX2);
		
		RES <= RESULT;
		
END implementation;