LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY full_adder IS
	PORT(	X, Y, C_in : IN STD_LOGIC;
			C_out, S	  : OUT STD_LOGIC);
END full_adder;

ARCHITECTURE implementation OF full_adder IS 
	BEGIN
	
		S <= (X XOR Y) XOR C_in ;
		C_out <= (X AND Y) OR (C_in AND X) OR (C_in AND Y) ;
END implementation;