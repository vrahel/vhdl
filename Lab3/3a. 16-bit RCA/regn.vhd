LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY regn IS
	GENERIC ( N : INTEGER := 4);
	PORT( R             : IN SIGNED(N-1 DOWNTO 0);
			Clock, Resetn : IN STD_LOGIC;
			Q             : OUT SIGNED(N-1 DOWNTO 0));
END regn;

ARCHITECTURE Behavior OF regn IS
	BEGIN
		Q <= (OTHERS =>'0') WHEN Resetn = '0' 
		ELSE R WHEN RISING_EDGE(Clock);
END Behavior;