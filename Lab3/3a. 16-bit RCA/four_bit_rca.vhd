LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY four_bit_rca IS
	PORT(	X, Y         : IN SIGNED(3 DOWNTO 0);
			CLOCK, RESET : IN STD_LOGIC;
			C_in	       : IN STD_LOGIC := '0';
			C_out        : OUT STD_LOGIC;
			RESULT       : OUT SIGNED(3 DOWNTO 0));
END four_bit_rca;

ARCHITECTURE four_bit_rca_implementation OF four_bit_rca IS 

	-- Components declarations
	COMPONENT regn IS
		GENERIC ( N : INTEGER := 4);
		PORT( R             : IN SIGNED(N-1 DOWNTO 0);
			   Clock, Resetn : IN STD_LOGIC;
			   Q             : OUT SIGNED(N-1 DOWNTO 0));
	END COMPONENT;

	COMPONENT full_adder IS
		PORT ( X, Y	 : IN STD_LOGIC;
				 C_in	 : IN STD_LOGIC;
				 C_out : OUT STD_LOGIC;
				 S		 : OUT STD_LOGIC);
	END COMPONENT;

	SIGNAL X_in, Y_in : SIGNED(3 DOWNTO 0); -- INPUTS
	SIGNAL CARRY : STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
	SIGNAL S : SIGNED(3 DOWNTO 0) := (others => '0');

	BEGIN
	
		-- Mapping ports' components
		REGX : regn PORT MAP(X, CLOCK, RESET, X_in);
		REGY : regn PORT MAP(Y, CLOCK, RESET, Y_in);
		REGS : regn PORT MAP(S, CLOCK, RESET, RESULT);
			
		FA0 : full_adder PORT MAP(X_in(0), Y_in(0), C_in, CARRY(0), S(0));
		FA1 : full_adder PORT MAP(X_in(1), Y_in(1), CARRY(0), CARRY(1), S(1));
		FA2 : full_adder PORT MAP(X_in(2), Y_in(2), CARRY(1), CARRY(2), S(2));
		FA3 : full_adder PORT MAP(X_in(3), Y_in(3), CARRY(2), C_out, S(3));
		
		
END four_bit_rca_implementation;