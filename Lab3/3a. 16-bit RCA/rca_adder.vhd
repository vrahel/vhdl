LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY rca_adder IS
	PORT( X, Y         : IN SIGNED(15 DOWNTO 0);
			CLOCK, RESET : IN STD_LOGIC;
		   C_out        : OUT STD_LOGIC;
		   S            : OUT SIGNED(15 DOWNTO 0));
END rca_adder;

ARCHITECTURE rca_adder_implementation OF rca_adder IS

	COMPONENT four_bit_rca is
		PORT( X, Y         : IN SIGNED(3 DOWNTO 0);
				CLOCK, RESET : STD_LOGIC;
			   C_in         : IN STD_LOGIC;
		   	C_out        : OUT STD_LOGIC;
				RESULT       : OUT SIGNED(3 DOWNTO 0));
	END COMPONENT;

	SIGNAL CARRY : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL C_in  : STD_LOGIC;

	BEGIN
	
		FOUR_BIT_RCA0 : four_bit_rca PORT MAP(X(3 DOWNTO 0), Y(3 DOWNTO 0), CLOCK, RESET, '0', CARRY(0), S(3 DOWNTO 0));
		FOUR_BIT_RCA1 : four_bit_rca PORT MAP(X(7 DOWNTO 4), Y(7 DOWNTO 4), CLOCK, RESET, CARRY(0), CARRY(1), S(7 DOWNTO 4));
		FOUR_BIT_RCA2 : four_bit_rca PORT MAP(X(11 DOWNTO 8), Y(11 DOWNTO 8), CLOCK, RESET, CARRY(1), CARRY(2), S(11 DOWNTO 8));
		FOUR_BIT_RCA3 : four_bit_rca PORT MAP(X(15 DOWNTO 12), Y(15 DOWNTO 12), CLOCK, RESET, CARRY(2), CARRY(3), S(15 DOWNTO 12));
		C_out <= CARRY(3);
		
END rca_adder_implementation;