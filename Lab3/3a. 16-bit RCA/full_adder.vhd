LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY FULL_ADDER IS
	PORT(	X,Y, C_in : IN STD_LOGIC;
			C_out, S	 : OUT STD_LOGIC);
END FULL_ADDER;

ARCHITECTURE IMPLEMENTATION OF FULL_ADDER IS 
	BEGIN
		S <= (X XOR Y) XOR C_in;
		C_out <= (X AND Y) OR (C_in AND X) OR (C_in AND Y);
END IMPLEMENTATION;