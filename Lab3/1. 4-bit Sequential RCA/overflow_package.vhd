LIBRARY ieee;
USE ieee.std_logic_1164.all;

PACKAGE overflow_package IS
	FUNCTION is_overflow ( CONSTANT C, C_next : IN STD_LOGIC) RETURN STD_LOGIC;
END overflow_package;

PACKAGE BODY overflow_package IS
	FUNCTION is_overflow ( CONSTANT C, C_next : STD_LOGIC) RETURN STD_LOGIC IS
		BEGIN
			RETURN (C XOR C_next);
	END;
END overflow_package;