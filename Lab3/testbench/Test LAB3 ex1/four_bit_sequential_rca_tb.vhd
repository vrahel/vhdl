LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

-- Include package overflow
LIBRARY work;  
USE work.overflow_package.all;  


ENTITY four_bit_sequential_rca_tb IS
	GENERIC ( N : INTEGER := 4);
	
END four_bit_sequential_rca_tb;



ARCHITECTURE behavior OF four_bit_sequential_rca_tb IS 

    -- Components declarations
	COMPONENT four_bit_sequential_rca IS
		PORT
		(	
			SW		           : IN SIGNED((2*N -1) DOWNTO 0); -- SW(3) AND SW(7) are the operands' signs
			KEY	           : IN STD_LOGIC_VECTOR(1 DOWNTO 0); -- RESET KEY(0); CLOCK KEY(1)
			LEDR             : OUT STD_LOGIC_VECTOR(9 DOWNTO 9); -- LEDR(9) is used to display an eventual overflow
			SUM   			  : OUT SIGNED(N-1 DOWNTO 0);
			HEX0, HEX1, HEX2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0) -- These show respectively the two operands and the sum
		); 
	END COMPONENT;
	
	
	-- Signals declarations
	--PORT SIGNALs
	SIGNAL SUM	  : SIGNED(N-1 DOWNTO 0); -- This is the final sum, output of the register
	SIGNAL SW	  : SIGNED(7 DOWNTO 0) := (OTHERS => '0');
	SIGNAL KEY 	  : STD_LOGIC_VECTOR(1 DOWNTO 0); --RESET AND CLOCK
	SIGNAL LEDR	  : STD_LOGIC_VECTOR(9 DOWNTO 9);
	SIGNAL HEX0,HEX1,HEX2: STD_LOGIC_VECTOR(6 DOWNTO 0);
	--/PORT SIGNALs
	
	
	SIGNAL X 	  : SIGNED(N-1 DOWNTO 0); -- Inputs X and Y are initalized to all zeroes
	SIGNAL Y 	  : SIGNED(N-1 DOWNTO 0);
	SIGNAL RESET  : STD_LOGIC := '0';
	SIGNAL CLOCK  : STD_LOGIC := '1';
	SIGNAL OFW	  : STD_LOGIC_VECTOR(9 DOWNTO 9);
	
   CONSTANT clk_period : time := 10 ns;

BEGIN
	
	SW(3 DOWNTO 0) <= X;
	SW(7 DOWNTO 4) <= Y;
	KEY(0) <= RESET;
	KEY(1) <= CLOCK;
	LEDR(9) <= OFW(9);
	
	-- Mapping ports' components
	RCA: four_bit_sequential_rca PORT MAP(SW, KEY, OFW, SUM, HEX0, HEX1, HEX2);


	clk_process : PROCESS -- This process sets the clock's waveform, with a 50% D.C.
	BEGIN
		CLOCK <= '0';
		WAIT FOR clk_period/2;
		CLOCK <= '1';
		WAIT FOR clk_period/2;
	END PROCESS;

	
	stim_proc: PROCESS -- Process used to test some cases of X, Y and RESET
	BEGIN 
		WAIT FOR 5 ns;
		
		RESET <= '1';
		X <= "0101"; -- +5
		Y <= "0101"; -- +5 = + 10, overflow
		WAIT FOR 15 ns;

		RESET <= '1';
		X <= "0011"; -- +3
		Y <= "1101"; -- - 3 = 0 
		WAIT FOR 15 ns;
		
		RESET <= '0';
		X <= "0101"; -- + 5
		Y <= "1101"; -- - 3
		WAIT FOR 15 ns;
		
		RESET <= '1';
		X <= "0101"; -- +5
		Y <= "1101"; -- -3
		
		WAIT;

		END PROCESS;

END;

