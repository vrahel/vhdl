LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

ENTITY rca_adder_tb is

END ENTITY;

ARCHITECTURE rca_adder_test OF rca_adder_tb IS	
	COMPONENT rca_adder IS
		PORT( X, Y         : IN SIGNED(15 DOWNTO 0);
				CLOCK, RESET : IN STD_LOGIC;
				S            : OUT SIGNED(15 DOWNTO 0));
	END COMPONENT;
		
	SIGNAL X, Y, S      : SIGNED(15 DOWNTO 0) := (OTHERS => '0');
	SIGNAL CLOCK, RESET : STD_LOGIC;
	CONSTANT clk_period : time := 10 ns;

	
	BEGIN
	
		-- Instantiating the RCA
		RCA16 : rca_adder PORT MAP(X, Y, CLOCK, RESET, S);
		
		clk_process : PROCESS -- Process used to create the clock waveform, with a 50% D.C.
		BEGIN
			CLOCK <= '0';
			WAIT FOR clk_period/2;
			CLOCK <= '1';
			WAIT FOR clk_period/2;
		END PROCESS;
		
		-- Stimulus process
		stim_proc : PROCESS
		BEGIN
			WAIT FOR 5 ns;
			RESET <= '1';
			X <= "1000000000000000"; -- 32768
			Y <= "0110000000001111"; -- 24576
			 
			WAIT FOR 10 ns;
			RESET <= '1';
			X <= "0000000000001111"; -- 15
			Y <= "0000000000001000"; -- 8

			WAIT FOR 5 ns;
			RESET <= '0';
			 
			WAIT FOR 5 ns;
			RESET <= '1';
			X <= "0000000000100001"; -- 33
			Y <= "0000000000000011"; -- 3
			WAIT;
		END PROCESS;
END rca_adder_test;