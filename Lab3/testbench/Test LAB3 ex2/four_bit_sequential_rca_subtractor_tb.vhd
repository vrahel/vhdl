LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_signed.all;

-- Include overflow package
LIBRARY work;  
USE work.overflow_package.all;  


ENTITY four_bit_sequential_adder_subtractor_tb IS
	GENERIC ( N: integer:= 4);
END four_bit_sequential_adder_subtractor_tb;

ARCHITECTURE behavior OF four_bit_sequential_adder_subtractor_tb IS 

    -- Components declarations
	 COMPONENT four_bit_add_sub IS
		PORT
		(	
			SW		           : IN SIGNED(2*N DOWNTO 0); -- SW(3) AND SW(7) are the operands' signs,  SW(8) IS ADD_SUB SIGNAL
			KEY	           : IN STD_LOGIC_VECTOR(1 DOWNTO 0); -- RESET KEY(0); CLOCK KEY(1)
			LEDR             : OUT STD_LOGIC_VECTOR(9 DOWNTO 9); -- LEDR(9) is used to display an eventual overflow
			RES   			  : OUT SIGNED(N-1 DOWNTO 0);
			HEX0, HEX1, HEX2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0) -- These show respectively the two operands and the sum
		); 
	END COMPONENT;
	
	
	-- Signals declarations
	-- Signals declarations
	--PORT SIGNALs
	SIGNAL RES    : SIGNED(N-1 DOWNTO 0); -- This is the final sum, output of the register
	SIGNAL SW	  : SIGNED(8 DOWNTO 0) := (OTHERS => '0');
	SIGNAL KEY 	  : STD_LOGIC_VECTOR(1 DOWNTO 0); --RESET AND CLOCK
	SIGNAL LEDR	  : STD_LOGIC_VECTOR(9 DOWNTO 9);
	SIGNAL HEX0, HEX1, HEX2: STD_LOGIC_VECTOR(6 DOWNTO 0);
	--/PORT SIGNALs
	
	SIGNAL X 	  : SIGNED(N-1 DOWNTO 0); -- Inputs X and Y are initalized to all zeroes
	SIGNAL Y 	  : SIGNED(N-1 DOWNTO 0);
	SIGNAL RESET  : STD_LOGIC := '0';
	SIGNAL CLOCK  : STD_LOGIC := '1';
	SIGNAL OFW	  : STD_LOGIC_VECTOR(9 DOWNTO 9);
	SIGNAL ADD_SUB: STD_LOGIC;
	
    CONSTANT clk_period : time := 10 ns;

BEGIN

	SW(3 DOWNTO 0) <= X;
	SW(7 DOWNTO 4) <= Y;
	SW(8) <= ADD_SUB;
	KEY(0) <= RESET;
	KEY(1) <= CLOCK;
	LEDR(9) <= OFW(9);

	ADD_SUB_RCA: four_bit_add_sub PORT MAP(SW, KEY, OFW, RES, HEX0, HEX1, HEX2);

	clk_process : PROCESS -- This process sets the clock's waveform, with a 50% D.C.
	BEGIN
		CLOCK <= '0';
		WAIT FOR clk_period/2;
		CLOCK <= '1';
		WAIT FOR clk_period/2;
	END PROCESS;

	
	stim_proc: PROCESS -- Process used to test some cases of X, Y, RESET and ADD_SUB
	BEGIN  
		WAIT FOR 12 ns;
		-- ADDING
		RESET <= '1';
		ADD_SUB <= '0';
		X <= "0101"; -- +5
		Y <= "0101"; -- +5 = + 10, overflow
		WAIT FOR 25 ns;

		-- SUBTRACTING
		RESET <= '1';
		ADD_SUB <= '1';
		X <= "0011"; -- +3
		Y <= "1101"; -- -3
		WAIT FOR 25 ns;
		
		-- RESETTING
		RESET <= '0';
		ADD_SUB <= '1';
		X <= "0101"; -- +5
		Y <= "1101"; -- -3
		wait for 25 ns;
	
		-- SUBTRACTING WITH OVERFLOW 5 - (-3) = 8, OUT OF RANGE 0-7
		RESET <= '1';
		ADD_SUB <= '1';
		X <= "0101"; -- +5
		Y <= "1101"; -- -3
		
		WAIT;

		END PROCESS;

END;

