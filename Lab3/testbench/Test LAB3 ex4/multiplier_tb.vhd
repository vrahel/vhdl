LIBRARY ieee;
USE ieee.numeric_std.all;
USE ieee.std_logic_1164.ALL;
USE IEEE.std_logic_signed.all;

ENTITY multiplier_tb IS
	GENERIC( N: INTEGER := 4);
END multiplier_tb;

ARCHITECTURE behavior OF multiplier_tb IS 

   -- Component declaration	 
	COMPONENT multiplier IS
		PORT
		( 
			SW : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			PRODUCT: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
			HEX0, HEX1, HEX2, HEX3 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)	
		);
	END COMPONENT;
	
	
	--SIGNALs DECLARATION
	--PORT SIGNALs
	SIGNAL SW: STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL PRODUCT: STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL HEX0, HEX1, HEX2, HEX3 : STD_LOGIC_VECTOR(6 DOWNTO 0);

	
	SIGNAL A, B: STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

	SW(3 DOWNTO 0) <= A;
	SW(7 DOWNTO 4) <= B;
	
	MULT: multiplier PORT MAP(SW, PRODUCT, HEX0, HEX1, HEX2, HEX3);
	
	-- Stimulus process
	stim_proc: PROCESS
	BEGIN

		WAIT FOR 5 ns;
		A <= "1111"; -- Checking the upper limit (15x15)
		B <= "1111";  
   
		WAIT FOR 10 ns;
		A <= "1111";
		B <= "0001"; --Checking if A*1 = A
		
		WAIT FOR 10 ns;
		A <= "0000";
		B <= "1010";-- Checking if B*0 = 0        
		
		WAIT FOR 10 ns;       
		A <= "0101"; -- 5
		B <= "1100"; -- 12                                  

		WAIT;
	END PROCESS;
	
END behavior;

