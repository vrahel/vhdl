LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_signed.all;

ENTITY select_adder_tb is

END ENTITY;

ARCHITECTURE test_select_adder of select_adder_tb is
	COMPONENT select_adder IS
		PORT( X, Y         : IN SIGNED(15 DOWNTO 0);
				CLOCK, RESET : IN STD_LOGIC;
			   S            : OUT SIGNED(15 DOWNTO 0));
	END COMPONENT;
		
	SIGNAL X, Y, S : SIGNED(15 DOWNTO 0) := (OTHERS => '0');
	SIGNAL CLOCK, RESET : STD_LOGIC;
	
	CONSTANT clk_period : time := 10 ns;
	
	BEGIN
	
		-- Instantiating select adder
		SA: select_adder PORT MAP (X, Y, CLOCK, RESET, S);
		
		clk_process : PROCESS
		BEGIN
			CLOCK <= '0';
			WAIT FOR clk_period/2;
			CLOCK <= '1';
			WAIT FOR clk_period/2;
		END PROCESS;
		
		-- Stimulus process
		stim_proc: PROCESS
		BEGIN
			WAIT FOR 5 ns;
			RESET <= '1';
			X <= "1000000000000000"; -- 32768
			Y <= "0110000000001111"; -- 24576
			 
			WAIT FOR 10 ns;
			X <= "0000000000000111"; -- 7
			Y <= "0000000000001000"; -- 8
			 
			WAIT FOR 10 ns;
			X <= "0000000000100001"; -- 33
			Y <= "0000000000000011"; -- 3
			WAIT;
		END PROCESS;

END test_select_adder;