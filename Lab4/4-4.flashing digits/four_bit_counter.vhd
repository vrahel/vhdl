LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY four_bit_tff_counter IS
    PORT 
	 (
		CLOCK   : IN STD_LOGIC;
		RESET   : IN STD_LOGIC;
		T       : IN STD_LOGIC; -- ENABLE PORT
		EN_NEXT : OUT STD_LOGIC; -- NEEDED TO CONNECT TO THE ENABLE SIGNAL OF NEXT 4BIT COUNTER
		Q       : OUT STD_LOGIC_vECTOR(3 DOWNTO 0)
    );
END four_bit_tff_counter;

ARCHITECTURE four_bit_tff_counter_implementation OF four_bit_tff_counter IS
	COMPONENT t_ff IS
		 PORT
		 (
			CLOCK : IN STD_LOGIC;
			RESET : IN STD_LOGIC;
			T	   : IN STD_LOGIC;
			Q	   : INOUT STD_LOGIC
		 );
	END COMPONENT;
		
	SIGNAL ENABLE : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL Q_out  : STD_LOGIC_VECTOR(3 DOWNTO 0);

	BEGIN
		
		-- MAPPING AND GATES
		
		ENABLE(0) <= Q_out(0) AND T;
		ENABLE(1) <= Q_out(1) AND ENABLE(0);
		ENABLE(2) <= Q_out(2) AND ENABLE(1); 
		ENABLE(3) <= Q_out(3) AND ENABLE(2);
		
		T_FF0: t_ff PORT MAP(CLOCK, RESET, T, Q_out(0));
		T_FF1: t_ff PORT MAP(CLOCK, RESET, ENABLE(0), Q_out(1));
		T_FF2: t_ff PORT MAP(CLOCK, RESET, ENABLE(1), Q_out(2));
		T_FF3: t_ff PORT MAP(CLOCK, RESET, ENABLE(2), Q_out(3));
		
		EN_NEXT <= ENABLE(3); -- THIS WILL BE THE ENABLE INPUT OF THE NEXT 4 BIT COUNTER
		Q <= Q_out;
		
END four_bit_tff_counter_implementation;