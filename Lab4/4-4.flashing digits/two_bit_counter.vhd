LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY two_bit_tff_counter IS
    PORT 
	 (
		CLOCK   : IN STD_LOGIC;
		RESET   : IN STD_LOGIC;
		T       : IN STD_LOGIC; -- ENABLE PORT
		EN_NEXT : OUT STD_LOGIC; -- NEEDED TO CONNECT TO THE ENABLE SIGNAL OF NEXT 4BIT COUNTER
		Q       : OUT STD_LOGIC_vECTOR(1 DOWNTO 0)
    );
END two_bit_tff_counter;

ARCHITECTURE two_bit_tff_counter_implementation OF two_bit_tff_counter IS
	COMPONENT t_ff IS
		 PORT
		 (
			CLOCK : IN STD_LOGIC;
			RESET : IN STD_LOGIC;
			T	   : IN STD_LOGIC;
			Q	   : INOUT STD_LOGIC
		 );
	END COMPONENT;
		
	SIGNAL ENABLE : STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL Q_out  : STD_LOGIC_VECTOR(1 DOWNTO 0);

	BEGIN
		
		-- MAPPING AND GATES
		
		ENABLE(0) <= Q_out(0) AND T;
		ENABLE(1) <= Q_out(1) AND ENABLE(0);

		
		T_FF0: t_ff PORT MAP(CLOCK, RESET, T, Q_out(0));
		T_FF1: t_ff PORT MAP(CLOCK, RESET, ENABLE(0), Q_out(1));

		
		EN_NEXT <= ENABLE(1); -- THIS WILL BE THE ENABLE INPUT OF THE NEXT 2 BIT COUNTER
		Q <= Q_out;
		
END two_bit_tff_counter_implementation;