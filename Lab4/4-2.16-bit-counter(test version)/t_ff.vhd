LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY t_ff IS
    PORT( CLOCK : IN STD_LOGIC;
		    RESET : IN STD_LOGIC;
		    T	    : IN STD_LOGIC;
		    Q	    : OUT STD_LOGIC);
END t_ff;

ARCHITECTURE behavioral OF t_ff IS

    SIGNAL q_reg  : STD_LOGIC;
    SIGNAL q_next : STD_LOGIC;
	 
	BEGIN

	T_FF: PROCESS(CLOCK, RESET)
		BEGIN
			IF RESET = '1' THEN
				q_reg <= '0'; 
         ELSIF RISING_EDGE(CLOCK) THEN
            q_reg <= q_next;
         END IF;
      END PROCESS;

INPUT_MUX:    
    q_next <= q_reg WHEN T = '0' ELSE NOT q_reg;
OUTPUT:
    Q  <= q_reg;

END behavioral;