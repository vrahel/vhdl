LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY tff_counter IS
	PORT( CLOCK  : IN STD_LOGIC;
         ENABLE : IN STD_LOGIC;
			RESET  : IN STD_LOGIC;
         Q  	 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
END tff_counter;

ARCHITECTURE foo OF tff_counter IS
	COMPONENT t_ff IS
		 PORT( CLOCK : IN STD_LOGIC;
			    RESET : IN STD_LOGIC;
			    T	    : IN STD_LOGIC;
			    Q 	 : OUT STD_LOGIC);
	END COMPONENT;
	
   SIGNAL T   : STD_LOGIC_VECTOR(0 TO 15);
	SIGNAL cnt : STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	BEGIN
			
		-- The seven AND gates:
		T <=  ( 
					ENABLE, ENABLE AND cnt(0), t(1) AND cnt(1), t(2) AND cnt(2), t(3) AND cnt(3),  
					t(4) AND cnt(4), t(5) AND cnt(5), t(6) AND cnt(6), t(7) AND cnt(7), t(8) AND cnt(8),  
					t(9) AND cnt(9), t(10) AND cnt(10), t(11) AND cnt(11), t(12) AND cnt(12), t(13) AND cnt(13), t(14) AND cnt(14)
				);

		GEN_TFF: 
		FOR I IN 0 TO 15 GENERATE
			tff_lab: t_ff PORT MAP(CLOCK, RESET, t(i), cnt(i));
		END GENERATE GEN_TFF;

		Q <= cnt;
		
END foo;