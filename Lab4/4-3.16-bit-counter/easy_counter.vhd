LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY easy_counter IS
   PORT( CLOCK : IN STD_LOGIC;
			RESET : IN STD_LOGIC;
			Q	   : OUT UNSIGNED(15 DOWNTO 0));
END easy_counter;

ARCHITECTURE Behavioral OF easy_counter IS

   SIGNAL temp : UNSIGNED(15 DOWNTO 0);
	
	BEGIN
	
	PROCESS(CLOCK, RESET)
		BEGIN
		
			IF RESET ='1' THEN
				temp <= "0000000000000000";
				
			ELSIF(RISING_EDGE(CLOCK)) THEN
				IF temp = "1111111111111111" THEN
						temp <= "0000000000000000";
				ELSE
					temp <= temp + 1;
				END IF;
			END IF;
   END PROCESS;
	
   Q <= temp;
	
END Behavioral;