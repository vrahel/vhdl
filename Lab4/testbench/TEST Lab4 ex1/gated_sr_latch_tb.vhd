LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY gated_sr_latch_tb IS

END gated_sr_latch_tb;

ARCHITECTURE Behavior OF gated_sr_latch_tb IS

	COMPONENT gated_sr_latch IS
		PORT ( Clk, R, S : IN STD_LOGIC;
		       Q         : OUT STD_LOGIC);
	END COMPONENT;
	
	SIGNAL CLOCK, R, S, Q : STD_LOGIC;
	CONSTANT clk_period : TIME := 20 ns;
	
	BEGIN
		
		SR: gated_sr_latch PORT MAP(CLOCK, R, S, Q);
		
		clk_process: PROCESS
			BEGIN
			CLOCK <= '0';
			WAIT FOR clk_period/2;
			CLOCK <= '1';
			WAIT FOR clk_period/2;
			END PROCESS;
			
		stim_proc: PROCESS
		BEGIN
			R <= '0';
			S <= '0';
			WAIT FOR 20 ns;
			R <= '0';
			S <= '1';
			WAIT FOR 20 ns;
			R <= '1';
			S <= '0';
			WAIT FOR 20 ns;
			R <= '1';
			S <= '1';
			WAIT;
		END PROCESS;
END Behavior;