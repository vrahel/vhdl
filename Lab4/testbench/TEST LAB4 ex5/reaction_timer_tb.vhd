library ieee;
use ieee.std_logic_1164.all;

entity reaction_control_tb is

end reaction_control_tb;

architecture reaction_control_implementation of reaction_control_tb is
	--COMPONENTs DECLARATION
	COMPONENT reset_entity IS
		PORT
		(
			SW		: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			ENABLE: IN STD_LOGIC;
			RESET : IN STD_LOGIC;
			CLOCK	: IN STD_LOGIC;
			ENABLE_CIRCUIT: OUT STD_LOGIC
		);
	END COMPONENT;
	
	COMPONENT display_counter IS
		PORT
		(
			ENABLE:IN STD_LOGIC;
			RESET :IN STD_LOGIC;
			FREEZE 	:IN STD_LOGIC;--KEY3 REACTION BUTTON
			CLOCK 	:IN STD_LOGIC;
			OUTPUT 	:OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
		);

	END COMPONENT;

	SIGNAL SW : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL KEY: STD_LOGIC_VECTOR(3 DOWNTO 0);
	
	CONSTANT clk_period : time := 20 ns;
	SIGNAL CLOCK_50: STD_LOGIC;	

	--SIGNALs DECLARATION
	
	--RESET_ENTITY SIGNALs
	SIGNAL ENABLE_CIRCUIT: STD_LOGIC;
	SIGNAL ENABLE_RS: STD_LOGIC := '0';
	SIGNAL RESET_RS: STD_LOGIC := '1';
	
	--DISPLAY_COUNTER SIGNALs
	SIGNAL OUTPUT   : STD_LOGIC_VECTOR(15 DOWNTO 0) := (OTHERS => '0');
	SIGNAL Q_DIGITS : STD_LOGIC_vECTOR(15 DOWNTO 0);
	SIGNAL RESET_DC : STD_LOGIC := '1';
	
	
	BEGIN
		
		IS_KEY_ZERO_PRESSED: PROCESS(KEY(0), ENABLE_CIRCUIT, RESET_RS)
		BEGIN
			IF(KEY(0) = '0') THEN
				ENABLE_RS <= '1';
				RESET_RS <= '0';
				RESET_DC <= '1';
			ELSE
				RESET_RS <= ENABLE_CIRCUIT AND '1';
				ENABLE_RS <= NOT(RESET_RS);
				RESET_DC <= '0';
			END IF;
		END PROCESS;
		

		DELAY_START: reset_entity PORT MAP(SW, ENABLE_RS, RESET_RS, CLOCK_50, ENABLE_CIRCUIT);
		
		
		KEY_THREE_PRESSED: PROCESS(KEY(3)) 
		BEGIN
			IF(KEY(3) = '0') THEN
				OUTPUT <= Q_DIGITS;
			END IF;
		END PROCESS;	
		
		START_COUNTING: display_counter PORT MAP(ENABLE_CIRCUIT, RESET_DC, KEY(3), CLOCK_50, Q_DIGITS);
		
		SW <= "00000010"; --1ms before pushing KEY(0)
			
		clk_process :process
		begin
			CLOCK_50 <= '0';
			wait for clk_period/2;
			CLOCK_50 <= '1';
			wait for clk_period/2;
		end process;

		
		stim_proc: process
		begin  
			KEY(0) <= '0';
			wait for 1 ms;
			
			wait for 300 ms; --press the button key(3) after 300 ms
			KEY(3) <= '0';
			
			wait for 200 ms;
			
			wait;
	end process;
		
end reaction_control_implementation;