LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY flashing_digits_tb IS
		
END flashing_digits_tb;

ARCHITECTURE flashing_digits_implementation OF flashing_digits_tb IS

	-- COMPONENTs DECLARATION
	COMPONENT four_bit_tff_counter IS
		 PORT 
		 (
			CLOCK   : IN STD_LOGIC;
			RESET   : IN STD_LOGIC;
			T       : IN STD_LOGIC; -- ENABLE PORT
			EN_NEXT : OUT STD_LOGIC; -- NEEDED TO CONNECT TO THE ENABLE SIGNAL OF NEXT 4BIT COUNTER
			Q       : OUT STD_LOGIC_vECTOR(3 DOWNTO 0)
		 );
	END COMPONENT;

	-- SIGNAL DECLARATION
	SIGNAL EN_NEXT            : STD_LOGIC_VECTOR(5 DOWNTO 0);
	SIGNAL Q                  : STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL ENABLE_DIGITS      : STD_LOGIC;
	SIGNAL Q_DIGIT            : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL ENABLE_NEXT_DIGITS : STD_LOGIC; -- NEEDED TO REUSE THE 4BIT COUNTER, NOT THE BEST SOLUTION
	
	CONSTANT clk_period : TIME := 60 ns;
	SIGNAL CLOCK_50: STD_LOGIC;

	BEGIN
	
		FOUR_BIT_COUNTER0: four_bit_tff_counter PORT MAP(CLOCK_50, '0', '1', EN_NEXT(0), Q(3 DOWNTO 0));
		FOUR_BIT_COUNTER1: four_bit_tff_counter PORT MAP(CLOCK_50, '0', EN_NEXT(0), EN_NEXT(1), Q(7 DOWNTO 4));
		FOUR_BIT_COUNTER2: four_bit_tff_counter PORT MAP(CLOCK_50, '0', EN_NEXT(1), EN_NEXT(2), Q(11 DOWNTO 8));
		FOUR_BIT_COUNTER3: four_bit_tff_counter PORT MAP(CLOCK_50, '0', EN_NEXT(2), EN_NEXT(3), Q(15 DOWNTO 12));
		FOUR_BIT_COUNTER4: four_bit_tff_counter PORT MAP(CLOCK_50, '0', EN_NEXT(3), EN_NEXT(4), Q(19 DOWNTO 16));
		FOUR_BIT_COUNTER5: four_bit_tff_counter PORT MAP(CLOCK_50, '0', EN_NEXT(4), EN_NEXT(5), Q(23 DOWNTO 20));
		
		-- ENABLE SIGNAL FOR THE DIGIT DISPLAYED ON HEX0
		ENABLE_DIGITS <= Q(0) AND Q(1) AND Q(2) AND Q(3) AND Q(4) AND Q(5) AND Q(6) AND Q(7) AND Q(8) AND Q(9) AND Q(10) AND Q(11) AND Q(12) AND Q(13) AND Q(14) AND Q(15) AND Q(16) AND Q(17) AND Q(18) AND Q(19) AND Q(20) AND Q(21) AND Q(22) AND Q(23);
		
		FOUR_BIT_COUNTER_DIGITS: four_bit_tff_counter PORT MAP(CLOCK_50, '0', ENABLE_DIGITS, ENABLE_NEXT_DIGITS, Q_DIGIT);
		
	clk_process: PROCESS
	BEGIN
		CLOCK_50 <= '0';
		WAIT FOR clk_period/2;
		CLOCK_50 <= '1';
		WAIT FOR clk_period/2;
	END PROCESS;

	stim_proc: PROCESS
	BEGIN  
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait for 30 ns;
		wait --add more wait statement or increase clock freq to check more values
	

	END PROCESS;
		
END flashing_digits_implementation;