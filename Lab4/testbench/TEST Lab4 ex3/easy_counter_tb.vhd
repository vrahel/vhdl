LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY easy_counter_tb IS

END easy_counter_tb;

ARCHITECTURE easy_counter_implementation OF easy_counter_tb IS

	COMPONENT easy_counter IS
		PORT( CLOCK : IN STD_LOGIC;
		      RESET : IN STD_LOGIC;
		      Q     : OUT UNSIGNED(15 DOWNTO 0));
	END COMPONENT;

	SIGNAL CLOCK : STD_LOGIC := '0';
	SIGNAL RESET : STD_LOGIC := '0';
	SIGNAL Q     : UNSIGNED(15 DOWNTO 0);

	CONSTANT clk_period : TIME := 20 ns;

	BEGIN

		counter: easy_counter PORT MAP(CLOCK, RESET, Q);

		clk_process: PROCESS
		BEGIN
			CLOCK <= '0';
			WAIT FOR clk_period/2;
			CLOCK <= '1';
			WAIT FOR clk_period/2;
		END PROCESS;

		stim_proc: PROCESS
		BEGIN
			WAIT FOR 5 ns;

			RESET <= '1';
			WAIT FOR 5 ns;

			RESET <= '0';
			WAIT;
		END PROCESS;
END easy_counter_implementation;