LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY tff_counter_tb IS

END tff_counter_tb;

ARCHITECTURE tff_counter_implementation OF tff_counter_tb IS
	COMPONENT tff_counter IS
		 PORT( CLOCK  : IN STD_LOGIC;
         ENABLE : IN STD_LOGIC;
			RESET  : IN STD_LOGIC;
         Q  	 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
	END COMPONENT;
	
	SIGNAL CLOCK  : STD_LOGIC := '0';
	SIGNAL ENABLE : STD_LOGIC := '0';
	SIGNAL RESET  : STD_LOGIC := '0';
	SIGNAL Q      : STD_LOGIC_VECTOR(15 DOWNTO 0);

	CONSTANT clk_period : time := 20 ns;
	
	BEGIN

		tff_count0: tff_counter PORT MAP(CLOCK, ENABLE, RESET, Q);

		clk_process: PROCESS
		BEGIN
			CLOCK <= '0';
			WAIT FOR clk_period/2;
			CLOCK <= '1';
			WAIT FOR clk_period/2;
		END PROCESS;

		stim_proc: PROCESS
		BEGIN
			WAIT FOR 5 ns;

			RESET <= '1';
			WAIT FOR 5 ns;

			RESET <= '0';
			ENABLE <= '1';
			WAIT;
		END PROCESS;
END tff_counter_implementation;