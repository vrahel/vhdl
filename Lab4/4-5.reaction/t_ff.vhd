library ieee;
use ieee.std_logic_1164.all;

entity t_ff is
	PORT
	(
		CLOCK: IN STD_LOGIC;
		RESET: IN STD_LOGIC;
		T	  : IN STD_LOGIC;
		Q	  : INOUT STD_LOGIC := '0'
	);
end t_ff;

architecture Behavioral of t_ff is
begin
	process (CLOCK, T, RESET)
	begin
		if CLOCK'event and CLOCK = '1' then
			if RESET = '1' then
				Q <= '0';
			elsif t = '1' then
				Q <= not Q;
			end if;
		end if;
	end process;
end Behavioral;