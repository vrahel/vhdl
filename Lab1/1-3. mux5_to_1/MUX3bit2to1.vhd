LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- Implementation of a 3 bit, 2 to 1 Multiplexer
ENTITY MUX3bit2to1 IS
	PORT(	A,B	 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
			S		 : IN STD_LOGIC;
			M_MUX3 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
END MUX3bit2to1;

ARCHITECTURE MUX2_TO_1_ARRAY OF MUX3bit2to1 IS
	COMPONENT MUX2to1comp
		PORT(	X,Y:	IN STD_LOGIC;
				S	:	IN STD_LOGIC;
				M	:	OUT STD_LOGIC);
	END COMPONENT;
	
	BEGIN
	-- Implementing a single 3 bit wide 2_to_1 MUX 
	-- Using 3 elementary 2_to_1 MUX and assigning port name: A, B are the 2 bit inputs, and M_MUX3 is the OUTPUT for the 2_to_1 BLOCK
	MUX2_TO_1_ARRAY_LOOP:
		FOR I IN 2 DOWNTO 0 GENERATE
		   MUX2_TO_1_ELEMENT: MUX2to1comp PORT MAP(A(I), B(I), S, M_MUX3(I));
		END GENERATE MUX2_TO_1_ARRAY_LOOP;
END MUX2_TO_1_ARRAY;