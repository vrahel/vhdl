LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- Implementation of a 5 to 1 Multiplexer
ENTITY MUX5 IS
   PORT ( SW   : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
			 LEDR : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
END MUX5;

ARCHITECTURE Behavior OF MUX5 IS
	COMPONENT MUX3bit2to1
		PORT ( A, B	  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
				 S      : IN STD_LOGIC;
				 M_MUX3 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
	END COMPONENT;

SIGNAL X,Y        : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL m0, m1, m2 : STD_LOGIC_VECTOR(2 DOWNTO 0); -- Intermediate MUX outputs
SIGNAL S          : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL m          : STD_LOGIC_VECTOR(2 DOWNTO 0); -- Final MUX output
SIGNAL u, v, w    : STD_LOGIC_VECTOR(2 DOWNTO 0);

BEGIN
	-- u, v and w are set as fixed inputs, chosen arbitrarily by us
	u <= "001";                
	v <= "010";
	w <= "100";
	
	-- Assignment of inputs, selection and output to switches and LEDs
	X <= SW(2 DOWNTO 0);
	Y <= SW(5 DOWNTO 3);
	S <= SW(8 DOWNTO 6);
	LEDR(2 DOWNTO 0) <= m;
	
	-- We are using three instances of 3 bit, 2 to 1 Multiplexers
	MUX0: MUX3bit2to1 PORT MAP (u, v, S(0), m0); 
	MUX1: MUX3bit2to1 PORT MAP (w, x, S(0), m1);
	MUX2: MUX3bit2to1 PORT MAP (m0, m1, S(1), m2);
	MUX3: MUX3bit2to1 PORT MAP (m2, y, S(2), m);	
END Behavior;