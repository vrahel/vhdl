LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY MUX2to1comp IS -- Basic 2 to 1 Multiplexer, with 1 bit inputs and outputs
	PORT (A,B: IN STD_LOGIC;
			S	: IN STD_LOGIC;
			M	: OUT STD_LOGIC);
END MUX2to1comp;

ARCHITECTURE MUX2to1comp_IMPLEMENTATION OF Mux2to1comp IS
	BEGIN
		M <= (NOT (S) AND A) OR (S AND B); -- Implementation of the basic "sum of products" function
END MUX2to1comp_IMPLEMENTATION;