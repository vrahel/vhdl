LIBRARY ieee;
USE ieee.std_logic_1164.all;
-- Simple module that connects 
-- The SW switches to the LEDR lights 
 
ENTITY ControllingLEDs IS 
      PORT ( SW   : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		       LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)); -- Red LEDs
END ControllingLEDs; 
 
ARCHITECTURE Behavior OF ControllingLEDs IS 
BEGIN
      LEDR <= SW;
END Behavior;